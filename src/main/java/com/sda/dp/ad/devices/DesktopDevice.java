package com.sda.dp.ad.devices;

import com.sda.dp.ad.Device;

public class DesktopDevice implements Device {
    private String ip;


    public boolean isOn() {
        return true;
    }

    public void turnOn() {
    }

    public void setParameter(String what, Object toWhat) {
        if (what.equals("ip")) {
            ip = String.valueOf(toWhat);
        }
    }
}
