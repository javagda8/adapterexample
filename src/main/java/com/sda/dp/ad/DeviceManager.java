package com.sda.dp.ad;

import java.util.List;

public class DeviceManager {
    private List<Device> deviceList;

    public void turnAllOn() {
        for (Device dev : deviceList) {
            dev.turnOn();
        }
    }
}
