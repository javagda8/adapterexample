package com.sda.dp.ad;

public interface Device {
    boolean isOn();

    void turnOn();

    void setParameter(String what, Object toWhat);

}
