package com.sda.dp.ad.otherdevices;

public class WatchDevice {
    private boolean turnedOn;
    private String reminder;
    private int batteryLevel;

    public boolean isTurnedOn() {
        return turnedOn;
    }

    public void setOnOff(boolean to) {
        turnedOn = to;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    public int getBatteryLevel(){
        return batteryLevel;
    }
}
