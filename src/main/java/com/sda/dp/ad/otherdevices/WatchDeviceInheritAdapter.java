package com.sda.dp.ad.otherdevices;

import com.sda.dp.ad.Device;

public class WatchDeviceInheritAdapter extends WatchDevice implements Device {
    public boolean isOn() {
        return super.isTurnedOn();
    }

    public void turnOn() {
        super.setOnOff(true);
    }

    public void setParameter(String what, Object toWhat) {
        if(what.equals("reminder")){
            super.setReminder(String.valueOf(toWhat));
        }
    }
}
