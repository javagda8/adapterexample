package com.sda.dp.ad;

public abstract class DeviceAbstract {
    public abstract boolean isOn();

    public abstract void turnOn();

    public abstract void setParameter(String what, Object toWhat);

    public int getBatteryLevel() {
        return 0;
    }
}
